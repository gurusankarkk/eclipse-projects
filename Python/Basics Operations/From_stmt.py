'''
Created on 25-Jul-2015

@author: Guru
'''
from Module1 import x,printer,l,add        # to import specific attributes from a module 
#printer()                   #if we use from we don't need to use module name qualifier,all the attributes and methods are
print x
x=8
l[0]=5
print x
print l
from Module1 import *
print x
print l     #we can see that the list is now mutated
add()            #variable x is not changed inside module1