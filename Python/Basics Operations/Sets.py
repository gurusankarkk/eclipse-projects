'''
Created on 18-Jul-2015

@author: Guru
'''
#sets are mutable
#forzensets are immutable variants of sets
X = set('spam')
Y = {'h', 'a', 'm'}
print X,Y
print X & Y
print X | Y
list1=list(set([1, 2, 1, 3, 1]))    #used to remove Duplicates
print list1
print 'p' in set('spam'), 'p' in 'spam', 'ham' in ['eggs', 'spam', 'ham']
print set('spam') - set('ham')
print set('ham') - set('spam')
print set('spam') == set('asmp')