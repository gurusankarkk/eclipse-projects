'''
Created on 18-Jul-2015

@author: Guru
'''
a=3
b=a             # now and b are referring to the same memory
print a
print b
a="spam"
print a
print b         # b still holds the value 3

#shared references won't hold for lists and dictionaries since they are mutable'

#shared references and equality
#small integers and strings are cached and reused, though, is tells us they reference the same single object.
x=1
y=1
print x==y
print x is y  #check in lists.py,this won't hold
import sys
print sys.getrefcount(1)