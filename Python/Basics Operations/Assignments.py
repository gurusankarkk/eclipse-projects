'''
Created on 19-Jul-2015

@author: Guru
'''
a,b,c,d="spam"
print c
#works with Python verison 3.X
#a,*b="spam"    a contains "s" and remaining "pam" is stored in b as List

#multi target Assignment
a=b="Guru"
print a,b
a=1;b=2;c=[a,b]
print c
a=4;print c
c=a,b;print c        #creates an tuple
L=[1,2,3]
L = L.append(4) #  append returns None, not L
print(L)
# Printing the hard way
import sys 
sys.stdout.write('hello world\n')
temp = sys.stdout # Save for restoring later
sys.stdout = open('log.txt', 'a') # Redirect prints to a file
print('spam') # Prints go to file, not here
print(1, 2, 3)
sys.stdout.close() # Flush output to disk
sys.stdout = temp # Restore original stream
print('back here') # Prints show up here again
print(open('log.txt').read())
