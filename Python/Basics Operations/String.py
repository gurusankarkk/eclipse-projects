'''
Created on 18-Jul-2015

@author: Guru
'''
str1="Hello World"
#Sequence Operations
print len(str1)
print str1[1]
print str1[-1] + str1[-2]
print len(str1[-1:5])
print str1[1:3]      #Slice of S from offsets 1 through 2 (not 3)
print str1 *5
print str1[1:8:2]   # Skipping items
print str1[::-1]  #reverse a string
#Strings are immutable the following code will not work
#str1[1]= 'r'
str2=str1 + ' Good morning'
print str2
#Operations Specific for only Strings
s="Spam"
r=s.find('pa')
print r
s.replace('pa', 'XYZ')
sa=s.replace('pa','XYZ')
print s,sa
print s.upper()
print s.lower()
line="aaa,bbb,ccc"
out=line.split(",")
print out
line1=line + "\n"
print line1
line2=line1.rstrip()     # Remove whitespace characters on the right side
print line2
line3=line1.rstrip().split(',') # Combine two operations
print line3
#loops
for c in 'spam':
    print(c.upper()) 
    
print "spam".join(["first","second","third"])


#formatting via expressions
print 'That is %d %s bird!' % (1, 'dead')
print '%d %s %g you' % (1, 'spam', 4.0)
print '%s -- %s -- %s' % (42, 3.14159, [1, 2, 3])
print '%(qty)d more %(food)s' % {'qty': 1, 'food': 'spam'}
template = '%s, %s and %s' # Same via expression
print template % ('spam', 'ham', 'eggs')

#formatting via method calls
template = '{motto}, {pork} and {food}' # By keyword
print template.format(motto='spam', pork='ham', food='eggs')

#formatting via method call is recommended over expressions
