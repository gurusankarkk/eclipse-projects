'''
Created on 19-Jul-2015

@author: Guru
'''
f = open('data.txt') # Read a four-line script file in this directory
print f.readline() # readline loads one line on each call
print f.readline()
print f.readline()
f = open('data.txt') # Read a four-line script file in this directory
print f.next() # readline loads one line on each call (__next__ in python 3.X)
print f.next()
#print f.next()   #uncomment to see StopIteration Exception

#using for loop to read file i.e it calls iterable __next__ method automatically without manually handling it
   #the below method is more effective since it uses iterator to read one line at a time
for line in open('data.txt'):
    print(line.upper())
   #this below one is ot recommended bcause the entire file is read and stored in memory  
for line in open('data.txt').readlines():
    print(line.upper())
    
#using while loop to do the same

f = open('data.txt')
while True:
    line = f.readline()
    if not line: break
    print(line.upper())

#Check in Book and notes to know more about iterators,since i have 2.X i can't try all possibles of 3.X 