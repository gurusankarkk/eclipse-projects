'''
Created on 18-Jul-2015

@author: Guru
'''
#dictionaries are also mutable
dict1={1:"One",2:"Two",3:"Three"}
print dict1[2]
dict1[4]="Four"   #dict1 is mutated
print dict1    
dict2= dict(name='Bob', job='dev', age=40)        #keywords
print dict2
dict3 = dict(zip(['name', 'job', 'age'], ['Bob', 'dev', 40])) # Zipping
print dict3
dict4= {'name': {'first': 'Bob', 'last': 'Smith'},
'jobs': ['dev', 'mgr'],
'age': 40.5}
print dict4['name']['last']
dict4['jobs'].append('janitor')
print dict4


#Missing Keys Test
if not 5 in dict1: # Python's sole selection statement
    print('missing')
    
keys1=list(dict1.keys()) 
print keys1

#understanding zip command
keys = ['spam', 'eggs', 'toast']
vals = [1, 3, 5]
list(zip(keys, vals))
D2 = {}
for (k, v) in zip(keys, vals): D2[k] = v
print D2
D3 = dict(zip(keys, vals))
print D3

#understanding enumerate
S = 'spam'
for (offset, item) in enumerate(S):
    print(item, 'appears at offset', offset)

E = enumerate(S)
print next(E)
print next(E)
