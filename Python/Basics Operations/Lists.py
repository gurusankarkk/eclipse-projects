'''
Created on 18-Jul-2015

@author: Guru
'''
#lists are mutable
#Sequence Operations
list1=[1,'hi',4.66]
print list1
print list1[0]
print list1[-1]
print list1[:-1]
list2=list1 + ['4',5,6]
print list2
list1[1]="Hello"
print list1
print list2  #Notice list2 still contains 'hi' instead of "Hello"

#type specific Opearions
list2.append("7th")
# list2.extend(["Ahh","Ohh"])   # to insert multiple items into List
print list2
list2.pop(2)
print list2
list2.sort()
print list2
list2.reverse()
print list2

#nested Lists
list3=[[1,2,3],[4,5,6],[7,8,9]]
print list3
print list3[0]        # we are extracting 1st row of a 3 X 3 Matrix
print list3[1][2]

# Basic Comprehensions 

list4=[row[1] for row in list3]   # we  can extract columns using this
print list4
list5=[row[1] for row in list3 if row[1] % 2 == 0]
print list5
list6 = [list3[i][i] for i in [0, 1, 2]]
print list6

#Range Function

list7=list(range(4))
print list7

list8= list(range(-6,9,2))
print list8

list9=[[x ** 2, x ** 3] for x in range(4)]
print list9

#aliasing
list9=[list1,8]
#copying lists
list12=[list1[:],8]
print list9
list1[1]="Hi"
print list9          #Change to List 1 gets affected in list9 because of aliasing
print list12         #Change to List 1 not affected  in list12 because of aliasing
#copying lists



list10=[1,2,3]
list11=[1,2,3]
print list10==list11
print list10 is list11