'''
Created on 18-Jul-2015

@author: Guru
'''
#Tuples are immutable
t1=(1,2,3,4)
print t1[1]
t2=(5,6,7,8)
t3=t1 + t2
print t3

#Type Specific

print t1.index(3)

#methods like append() won't work with tuples since they are immutable'